package com.example.myapplication.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.myapplication.R;
import com.google.android.material.snackbar.Snackbar;

public class MainFragment extends Fragment {

    // PLAYER 1
    // Poison
    private Button btn_PoisonUpP1;
    private Button btn_PoisonDownP1;
    // Steal Life
    private ImageButton imgBtn_StealP1;
    // Life Up & Down
    private ImageButton imgBtn_LifeUpP1;
    private ImageButton imgBtn_LifeDownP1;
    // Text Life
    private TextView txtView_LifeP1;

    // PLAYER 2
    // Poison
    private Button btn_PoisonUpP2;
    private Button btn_PoisonDownP2;
    // Steal Life
    private ImageButton imgBtn_StealP2;
    // Life Up & Down
    private ImageButton imgBtn_LifeUpP2;
    private ImageButton imgBtn_LifeDownP2;
    // Text Life
    private TextView txtView_LifeP2;

    // LIFE & POISON INT
    private int life1;
    private int life2;
    private int poison1;
    private int poison2;

    private View view;

    private MainViewModel mViewModel;
    Snackbar snackbar;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.main_fragment, container, false);

        declarations();
        reset();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){

                    // POISON BUTTONS
                    case R.id.btn_PoisonUpP1:
                        poison1++;
                        break;
                    case R.id.btn_PoisonUpP2:
                        poison2++;
                        break;
                    case R.id.btn_PoisonDownP1:
                        if (poison1>0) poison1--;
                        break;
                    case R.id.btn_PoisonDownP2:
                        if (poison2>0) poison2--;
                        break;

                    // LIFE BUTTONS
                    case R.id.imgBtn_LifeUpP1:
                        life1++;
                        break;
                    case R.id.imgBtn_LifeUpP2:
                        life2++;
                        break;
                    case R.id.imgBtn_LifeDownP1:
                        life1--;
                        break;
                    case R.id.imgBtn_LifeDownP2:
                        life2--;
                        break;

                    // STEAL LIFE
                    case R.id.imgBtn_StealP1:
                        life1++;
                        life2--;
                        break;
                    case R.id.imgBtn_StealP2:
                        life2++;
                        life1--;
                        break;
                }
                updateViews();
                if (life1<=0) {
                    snackbar = Snackbar.make(view, "VICTORIA: Player - 2", 7000);
                    snackbar.show();
                    reset();
                }
                if (life2<=0) {
                    snackbar = Snackbar.make(view, "VICTORIA: Player - 1", 7000);
                    snackbar.show();
                    reset();
                }
            }
        };


        // OnSaveInstanceState
        if (savedInstanceState != null) {
            life1 = (int) savedInstanceState.get("life1");
            life2 = (int) savedInstanceState.get("life2");
            poison1 = (int) savedInstanceState.get("poison1");
            poison2 = (int) savedInstanceState.get("poison2");
            updateViews();
        } else {
            reset();
        }

        // SAVED LISTENERS
        savedListenerers(listener);

        return view;
    }

    // SAVED LISTENERS
    private void savedListenerers(View.OnClickListener listener) {
        // PLAYER 1
        btn_PoisonUpP1.setOnClickListener(listener);
        btn_PoisonDownP1.setOnClickListener(listener);
        imgBtn_StealP1.setOnClickListener(listener);
        imgBtn_LifeUpP1.setOnClickListener(listener);
        imgBtn_LifeDownP1.setOnClickListener(listener);

        // PLAYER2
        btn_PoisonUpP2.setOnClickListener(listener);
        btn_PoisonDownP2.setOnClickListener(listener);
        imgBtn_StealP2.setOnClickListener(listener);
        imgBtn_LifeUpP2.setOnClickListener(listener);
        imgBtn_LifeDownP2.setOnClickListener(listener);
    }

    // RESETEA VALORES INICIALES
    private void reset() {
        life1 = 20;
        life2 = 20;
        poison1 = 0;
        poison2 = 0;
        
        updateViews();
    }

    // ACTUALIZA VALORES DE VIDA Y VENENO
    private void updateViews() {
        txtView_LifeP1.setText(life1+"/"+poison1);
        txtView_LifeP2.setText(life2+"/"+poison2);
    }

    private void declarations() {
        // POISON
        btn_PoisonDownP1 = view.findViewById(R.id.btn_PoisonDownP1);
        btn_PoisonDownP2 = view.findViewById(R.id.btn_PoisonDownP2);
        btn_PoisonUpP1 = view.findViewById(R.id.btn_PoisonUpP1);
        btn_PoisonUpP2 = view.findViewById(R.id.btn_PoisonUpP2);

        // STEAL LIFE
        imgBtn_StealP1 = view.findViewById(R.id.imgBtn_StealP1);
        imgBtn_StealP2 = view.findViewById(R.id.imgBtn_StealP2);

        // LIFE UP & DOWN
        imgBtn_LifeUpP1 = view.findViewById(R.id.imgBtn_LifeUpP1);
        imgBtn_LifeUpP2 = view.findViewById(R.id.imgBtn_LifeUpP2);
        imgBtn_LifeDownP1 = view.findViewById(R.id.imgBtn_LifeDownP1);
        imgBtn_LifeDownP2 = view.findViewById(R.id.imgBtn_LifeDownP2);

        // TEXT LIFE
        txtView_LifeP1 = view.findViewById(R.id.txtView_LifeP1);
        txtView_LifeP2 = view.findViewById(R.id.txtView_LifeP2);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putInt("life1", life1);
        outState.putInt("life2", life2);
        outState.putInt("poison1", poison1);
        outState.putInt("poison2", poison2);

        super.onSaveInstanceState(outState);

    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.men_refersh){
            reset();
            updateViews();
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }
}